using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;
using Snapshooter.NUnit;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using TestProject.Models;

namespace TestProject
{
    public class Tests
    {

        private HttpClient httpClient;

        [SetUp]
        public void Setup()
        {
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("https://localhost:44379");
        }

        [Test]
        public void Get_clients_response_is_correct()
        {
            // Get the response from the API
            var response = httpClient.GetAsync("/api/client").GetAwaiter().GetResult();
            // Assert OK response
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            // Capture JSON
            var result = response.Content.ReadAsStringAsync().Result;
            // Deserialize the response
            var client = JsonConvert.DeserializeObject<List<Client>>(result);
            // Snapshot the response & Compare the response to snapshot
            Snapshot.Match(client);

        }

        [Test]
        public void Get_individual_client_response_is_correct()
        {
            // Get the response from the API
            var response = httpClient.GetAsync(@"/api/client/95fff557-dc35-447d-bfd4-34d15e191bbd").GetAwaiter().GetResult();
            // Assert OK response
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            // Capture JSON
            var result = response.Content.ReadAsStringAsync().Result;
            // Deserialize the response
            var client = JsonConvert.DeserializeObject<Client>(result);
            // Snapshot the response & Compare the response to snapshot
            Snapshot.Match(client);

        }

        [Test]
        public void Get_invalid_individual_client_response_throws_error()
        {
            // Get the response from the API
            var response = httpClient.GetAsync(@"/api/client/invalid_id").GetAwaiter().GetResult();
            // Assert BadRequest response
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Test]
        public void Post_individual_client_and_delete()
        {
            // Questioning how you would do this one and not potentially mess up your data set if the delete did not run correctly

            // Create deserialized payload
            var payload = new Client
            {
                Title = "Mr",
                FirstName = "Test",
                LastName = "Input",
                Email = "test.input@mailinator.com",
                ContactDetails = new ContactDetails
                {
                    PhoneType = "Home",
                    PhoneNumber = "07123456789"
                },
            };
            // Serialize payload
            var stringPayload = JsonConvert.SerializeObject(payload);
            // Wrap our JSON inside a StringContent which then can be used by the HttpClient class
            var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");
            // Post the request to the API
            var response = httpClient.PostAsync(@"/api/client", httpContent).GetAwaiter().GetResult();
            // Assert OK response
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            // Capture JSON just added
            var result = response.Content.ReadAsStringAsync().Result;
            //// Deserialize the response
            var client = JsonConvert.DeserializeObject<Client>(result);
            //// Snapshot the response & Compare the response to snapshot
            Snapshot.Match(client, matchOptions => matchOptions.IgnoreField("Id"));

            // Post the delete to the API
            var deleteResponse = httpClient.DeleteAsync($"/api/client/{client.Id}").GetAwaiter().GetResult();
            // Assert OK response
            deleteResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        }
    }
}

