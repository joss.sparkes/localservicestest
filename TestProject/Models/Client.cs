﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestProject.Models
{
    public class Client
    {
        [Required()]
        public Guid Id { get; set; }

        [Required()]
        public string Title { get; set; }

        [Required()]
        public string FirstName { get; set; }

        [Required()]
        public string LastName { get; set; }
        
        [Required()]
        public string Email { get; set; }

        [Required()]
        public ContactDetails ContactDetails { get; set; }
    }
}
