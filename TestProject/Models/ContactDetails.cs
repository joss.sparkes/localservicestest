﻿namespace TestProject.Models
{
    public class ContactDetails
    {
        public string PhoneType { get; set; }

        public string PhoneNumber { get; set; }
    }
}
